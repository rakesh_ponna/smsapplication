package com.example.rakesh.smsapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by rakesh on 09/07/17.
 */

public class PhoneStateReceiver extends BroadcastReceiver {
    private static boolean ring=false;
    private  static boolean callReceived=false;
    private  String TAG = "PhoneStateReceiver";
    private String incomingNumber;
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            System.out.println("Receiver start");
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            // Get the Caller's Phone Number

            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            // Get the current Phone State

            if(state==null)
                return;

            // If phone state "Rininging"
            if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
            {
                ring =true;
                Log.d(TAG,"It was A Ringing CALL from : "+incomingNumber);
            }

            // If incoming call is received
            if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
            {
                callReceived=true;
                Log.d(TAG,"It was A Receivd CALL from : "+incomingNumber);

            }

            // If phone is Idle
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
            {
                // If phone was ringing(ring=true) and not received(callReceived=false) , then it is a missed call
                if(ring==true&&callReceived==false)
                {
                    sendSms(incomingNumber);
                    Log.d(TAG,"It was A MISSED CALL from : "+incomingNumber);
                 }
                if(ring==false&&callReceived==true)
                {
                    Log.d(TAG,"It was A Received CALL from : "+incomingNumber);
                }
            }
         }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sendSms(String incomingNumber){

        String lantlngSms =  "You can find me on " + "http://maps.google.com/?q=" + Constants.longitude + "," + Constants.longitude;
        Constants.smsSender = lantlngSms;
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(incomingNumber, null, lantlngSms, null, null);

    }



}
